import path from 'path';

import config from './webpack';

const ENV = process.env.NODE_ENV;

export default {
  mode: ENV === 'production' ? 'production' : 'development',
  entry: {
    app: [
      'webpack-hot-middleware/client',
      'react-hot-loader/patch',
      path.resolve(__dirname, 'src/app/index.js')
    ]
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'public'),
    publicPath: '/'
  },
  devtool: 'inline-source-map',
  devServer: {
    port: 9000,
    contentBase: './public',
    historyApiFallback: true,
    hot: true
  },
  module: {
    rules: config.rules
  },
  plugins: config.plugins
};
