import rules from './rules';
import plugins from './plugins';

export default {
  rules,
  plugins
};
