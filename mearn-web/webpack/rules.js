export default [
  {
    test: /\.(js|jsx)$/,
    exclude: /(node_modules)/,
    use: {
      loader: 'babel-loader',
      options: {
        presets: ['env', 'react', 'stage-2'],
        plugins: ['react-hot-loader/babel']
      }
    }
  },
  {
    test: /\.(scss|sass)$/,
    use: ['style-loader', 'css-loader', 'sass-loader']
  }
];
