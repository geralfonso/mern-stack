import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';

export default [
  new webpack.HotModuleReplacementPlugin(),
  new HtmlWebpackPlugin({
    inject: true,
    title: 'Demo de React',
    template: './src/shared/index.html',
    filename: './index.html'
  })
];
