import http from 'http';
import path from 'path';

// Mongo
import { mongoose, router as api } from 'mearn-db';
import express from 'express';
import webpack from 'webpack';
import open from 'open';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpackHotServerMiddleware from 'webpack-hot-server-middleware';

// Webpack Config
import webpackConfig from '../../webpack.config';

const port = process.env.PORT || 3000;

const app = express();
const server = http.createServer(app);
const compiler = webpack(webpackConfig);

// Hot Module Replacement
app.use(
  webpackDevMiddleware(compiler, {
    publicPath: webpackConfig.output.publicPath
  })
);
app.use(webpackHotMiddleware(compiler));

// Middleware
app.use(express.json());

// Routes
app.use('/api/tasks', api);

// Public Static
app.use(express.static(path.join(__dirname, './public')));

server.listen(port, err => {
  if (!err) {
    open(`http://localhost:${port}`);
  }
});
