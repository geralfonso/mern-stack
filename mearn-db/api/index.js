const { Router } = require('express');
const Task = require('../models/task');
const router = Router();

// Get all Tasks
router.get('/', async (req, res) => {
  const tasks = await Task.find();

  res.json(tasks);
});

// Get one Task
router.get('/:id', async (req, res) => {
  const task = await Task.findById(req.params.id);

  res.json(task);
});

// Create Task
router.post('/', async (req, res) => {
  const { title, description } = req.body;
  const task = new Task({ title, description });

  await task.save();
  res.json({ status: 'Task Saved' });
});

// Update Task
router.put('/:id', async (req, res) => {
  const { title, description } = req.body;
  const newTask = { title, description };

  await Task.findByIdAndUpdate(req.params.id, newTask);

  res.json({ status: 'Task Updated' });
});

// Delete Task
router.delete('/:id', async (req, res) => {
  await Task.findByIdAndRemove(req.params.id);

  res.json({ status: 'Task Deleted' });
});

module.exports = router;
