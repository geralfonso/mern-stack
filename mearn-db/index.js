const mongoose = require('mongoose');
const router = require('./api');

const URI = 'mongodb://localhost/mern-tasks';

mongoose
  .connect(
    URI,
    { useNewUrlParser: true }
  )
  .then(db => console.log('The DB is connected...'))
  .catch(err => console.log(err));

module.exports = { mongoose, router };
